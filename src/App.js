import React, { Component } from 'react';
import './App.css';

import NavigationNotLogged from './Components/NavigationNotLogged';
import NavigationLogged from './Components/NavigationLogged';

export default class App extends Component {

    isLogged()
    {
        return !!localStorage.user;
    }

    render() {

        if( ! this.isLogged() )
        {
            return (
                <div>
                    <NavigationNotLogged />
                    <div className="container">
                        {this.props.children}
                    </div>
                </div>
            );
        }

        return (
            <div>
                <NavigationLogged />
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}