import React, {Component} from 'react';
import {NavItem, Navbar, Nav} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';

export default class NavigationNotLogged extends Component
{
    render()
    {
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">React-Blog</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to='/login'>
                            <NavItem eventKey={1}>Login</NavItem>
                        </LinkContainer>
                        <LinkContainer to='/register'>
                            <NavItem eventKey={2}>Registro</NavItem>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}