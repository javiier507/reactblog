import React, {Component} from 'react';
import Form from 'react-formal';
import {modelSchema} from '../Schemas/Register';
import axios from 'axios';
import {hashHistory} from 'react-router';

export default class Login extends Component
{
    constructor()
    {
        super();
        this.state = {
            model: modelSchema,
            errorLogin: false
        }
    }

    onSubmit(formValues)
    {
        axios.post('http://localhost:5000/api/register', formValues.user)
            .then((response) => {
                return hashHistory.push('/login');
            })
            .catch((error) => {
                this.setState({
                    errorLogin: true
                })
            })
    }

    render()
    {
        const {errorLogin, model} = this.state;
        return (
            <div>
                {errorLogin === true &&
                    <div className="alert alert-danger">
                        Datos de login incorrectos
                    </div>
                }

                <Form
                    onSubmit={(e) => this.onSubmit(e)}
                    schema={modelSchema}
                    value={model}
                    onChange={modelInfo => this.setState({model: modelInfo})}
                >

                    <div style={{"marginTop" : "10px"}} className="form-group">
                        <label>Nombre</label>
                        <Form.Field className={`form-control col-md-12`} name='user.first_name'/>
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.first_name'/>
                    </div>

                    <br />

                    <div style={{"marginTop" : "10px"}} className="form-group">
                        <label>Apellido</label>
                        <Form.Field className={`form-control col-md-12`} name='user.last_name'/>
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.last_name'/>
                    </div>

                    <br />

                    <div style={{"marginTop" : "10px"}} className="form-group">
                        <label>Correo</label>
                        <Form.Field className={`form-control col-md-12`} name='user.email'/>
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.email'/>
                    </div>

                    <br />

                    <div className="form-group">
                        <label>Contraseña</label>
                        <Form.Field type="password" className={`form-control col-md-12`} name='user.password'/>
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.password'/>
                    </div>

                    <br />

                    <Form.Button style={{"marginTop" : "10px"}} className={`btn btn-block btn-success`} type='submit'>
                        Registrar
                    </Form.Button>
                </Form>
            </div>
        )
    }
}