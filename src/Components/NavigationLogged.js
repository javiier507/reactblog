import React, {Component} from 'react';
import {NavItem, Navbar, Nav} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import { hashHistory } from 'react-router'

export default class NavigationLogged extends Component
{
    onLogout()
    {
        window.localStorage.clear();
        hashHistory.push('/login');
    }

    render()
    {
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">React-Blog</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to='/profile'>
                            <NavItem eventKey={1}>Perfil</NavItem>
                        </LinkContainer>
                        <LinkContainer to='/posts'>
                            <NavItem eventKey={2}>Posts</NavItem>
                        </LinkContainer>
                    </Nav>
                    <Nav pullRight>
                        <NavItem eventKey={3} onClick={this.onLogout}>
                            Logout
                        </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
