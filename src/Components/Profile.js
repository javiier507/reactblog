import React, {Component} from 'react';
import {Panel} from 'react-bootstrap';

export default class Profile extends Component
{
    constructor()
    {
        super();
        this.state = {
            user: JSON.parse(localStorage.getItem('user'))
        }
    }

    render()
    {
        const {user} = this.state;

        return (
            <div>
                <Panel header={`Información del usuario con id : ${user._id}`} >
                    <p>Nombre: {user.first_name} </p>
                    <p>Apellido: {user.last_name} </p>
                    <p>Correo: {user.email} </p>
                </Panel>
            </div>
        )
    }
}