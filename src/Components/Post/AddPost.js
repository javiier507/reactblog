import React, { Component } from 'react';
import Form from 'react-formal';
import { modelSchema } from '../../Schemas/Post';
import axios from 'axios';
import { hashHistory } from 'react-router';

export default class AddPost extends Component {
    constructor() {
        super();
        this.state = {
            model: modelSchema
        }
    }

    onSubmit(formValues) {
        axios.post('http://localhost:5000/api/post', formValues.post)
            .then((response) => {
                return hashHistory.push('/posts');
            })
    }

    render() {
        return (
            <Form
                onChange={modelInfo => this.setState({ model: modelInfo})}
                onSubmit={this.onSubmit}
                value={this.state.model}
                schema={modelSchema}
            >
                <div style={{"marginTop": "10px"}} className="form-group" >
                    <label>Titulo</label>
                    <Form.Field className={`form-control col-md-12`} name='post.title' />
                    <Form.Message errorClass={`form-control alert alert-danger`} for='post.title' />
                </div>

                <div className="clearfix"></div>

                <div className="form-group">
                    <label>Contenido</label>
                    <Form.Field rows="6" type="textarea" className={`form-control col-md-12`} name='post.content' />
                    <Form.Message errorClass={`form-control alert alert-danger`} for='post.content' />
                </div>

                <div className="clearfix"></div>

                <Form.Button style={{"marginTop":"40px"}} className={`btn btn-block btn-success`} type="submit">
                    Crear Post
                </Form.Button>
            </Form>
        )
    }
}
