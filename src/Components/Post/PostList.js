import React, {Component} from 'react';
import axios from 'axios';
import Posts from './Posts';

export default class PostList extends Component {

    constructor() {
        super();

        this.state = {
            posts : []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/api/posts')
            .then((response) => {
                this.setState({posts: response.data })
            })
            .catch((response) => {

            })
    }

    render() {
        return(
            <Posts posts={this.state.posts} />
        )
    }
}
