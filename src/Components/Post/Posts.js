import React, {Component} from 'react';
import {Panel, Button} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import axios from 'axios';
import Post from './Post';

export default class Posts extends Component {

    onRemove(id) {
        const url = `http://localhost:5000/api/posts/${id}`;

        axios.delete(url)
            .then((response) => {
                window.location.reload();
            })
            .catch((response) => {
                console.log(response);
            })
    }

    render() {
        const {posts} = this.props;
        const addBtnPost = (
            <div className="pull-rigth">
                <LinkContainer to={`posts/add`}>
                    <Button block bsClass="btn btn-primary" bsStyle="default">
                        Crear Nuevo Post
                    </Button>
                </LinkContainer>
            </div>
        )

        if(posts.length === 0) {
            return (
                <div>
                    {addBtnPost}
                    <div className="clearfix"></div>
                    <hr />
                    <Panel bsStyle="danger" header="No Hay Post">
                        No Existen Posts
                    </Panel>
                </div>
            )
        }

        const createPost = (post, index) => {
            return (
                <Post key={index} post={post} onRemove={ this.onRemove.bind(this, post._id) } />
            )
        }

        return (
            <div>
                {addBtnPost}
                <div className="clearfix"></div>
                <hr />
                {posts.map(createPost)}
            </div>
        )
    }
}
