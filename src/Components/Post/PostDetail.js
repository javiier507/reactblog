import React from 'react'
import axios from 'axios'
import {Well, Button} from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap';

export default class PostDetail extends React.Component
{
    constructor()
    {
        super();
        this.state = {
            post: {}
        }
    }

    componentDidMount()
    {
        const url = `http://localhost:5000/api/posts/${this.props.params.id}`;
        axios.get(url)
            .then((response) => {
                this.setState({
                    post: response.data
                })
            })
    }

    render()
    {
        const {post} = this.state;
        return(
            <div>
                <h2 className="text-center text-muted">{post.title}</h2>
                <hr />

                <Well>
                    {post.content}
                    <hr />

                    <LinkContainer to='/posts'>
                        <Button block bsStyle="warning">Volver al listado</Button>
                    </LinkContainer>
                </Well>
            </div>
        )
    }
}
