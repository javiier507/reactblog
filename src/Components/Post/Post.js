import React, {Component} from 'react';
import {Panel, Button} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';

export default class Post extends Component {

    render() {

        const {post, onRemove} = this.props;

        return (
            <Panel
                bsStyle="primary"
                header={post.title}
                footer={
                    <div>
                        <LinkContainer to={`post/${post._id}`} >
                            <Button bsStyle="primary">Detalle</Button>
                        </LinkContainer>
                        <LinkContainer to={`post/edit/${post._id}`} >
                            <Button bsStyle="info">Editar</Button>
                        </LinkContainer>
                        <Button onClick={onRemove} bsStyle="danger">Eliminar</Button>
                    </div>
                }
            >
                {post.content}
            </Panel>
        );
    }
}
