import React, {Component} from 'react';
import Form from 'react-formal'
import {modelSchema} from '../../Schemas/Post'
import axios from 'axios';

export default class EditPost extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            model: modelSchema,
            post: null,
            updated: false
        }

        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount()
    {
        const url = `http://localhost:5000/api/posts/${this.props.params.id}`;

        axios.get(url)
            .then((response) => {
                this.setState({
                    post: response.data
                })
            })
            .catch((error) => {
                console.log(error);
            });
    }

    onSubmit(formValues)
    {
        const url = `http://localhost:5000/api/posts/${this.props.params.id}`;

        axios.put(url, formValues.post)
            .then((response) => {
                this.setState({
                    updated: true
                })
            })
            .catch((response) => {
                console.log(response);
            });
    }

    render()
    {
        const {post, updated} = this.state;

        if(!post) return (
            <h1>404, Post No Encontrado!</h1>
        )

        return (
            <div>
                {updated === true &&
                    <div className="alert alert-success">
                        Post Actualizado
                    </div>
                }

                <Form
                    onChange={modelInfo => this.setState({model: modelInfo})}
                    onSubmit={this.onSubmit}
                    defaultValue={{
                        post
                    }}
                    schema={modelSchema}
                >
                    <div style={{"marginTop": "10px"}} className="form-group" >
                        <label>Titulo</label>
                        <Form.Field className={`form-control col-md-12`} name='post.title' />
                        <Form.Message errorClass={`form-control alert alert-danger`} for='post.title' />
                    </div>

                    <div className="clearfix"></div>

                    <div className="form-group">
                        <label>Contenido</label>
                        <Form.Field rows="6" type="textarea" className={`form-control col-md-12`} name='post.content' />
                        <Form.Message errorClass={`form-control alert alert-danger`} for='post.content' />
                    </div>

                    <div className="clearfix"></div>

                    <Form.Button style={{"marginTop":"40px"}} className={`btn btn-block btn-success`} type="submit">
                        Crear Post
                    </Form.Button>
                </Form>

            </div>
        )
    }
}
