const loggedIn = () => {
    return !!localStorage.user;
}

const UserIsNotLogged = (nextState, replace) => {
    if(!loggedIn())
    {
        replace({
            pathname: '/login'
        })
    }
}

export default UserIsNotLogged;