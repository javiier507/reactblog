const loggedIn = () => {
    return !!localStorage.user;
}

const UserIsLogged = (nextState, replace) => {
    if(loggedIn())
    {
        replace({
            pathname: '/profile'
        })
    }
}

export default UserIsLogged;