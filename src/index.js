import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import Login from './Components/Login';
import Register from './Components/Register';
import Profile from './Components/Profile';

import PostList from './Components/Post/PostList';
import PostDetail from './Components/Post/PostDetail';
import AddPost from './Components/Post/AddPost';
import EditPost from './Components/Post/EditPost';

import UserIsLogged from './Auth/UserIsLogged';
import UserIsNotLogged from './Auth/UserIsNotLogged';

import { Router, Route, hashHistory, IndexRoute } from 'react-router';

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Login} onEnter={UserIsLogged} />
            <Route path="/login" component={Login} onEnter={UserIsLogged} />
            <Route path="/register" component={Register} onEnter={UserIsLogged} />
            <Route path="/profile" component={Profile} onEnter={UserIsNotLogged} />
            <Route path="/posts" component={PostList} onEnter={UserIsNotLogged} />
            <Route path="/post/:id" component={PostDetail} onEnter={UserIsNotLogged} />
            <Route path="/posts/add" component={AddPost} onEnter={UserIsNotLogged} />
            <Route path="/post/edit/:id" component={EditPost} onEnter={UserIsNotLogged} />
        </Route>
    </Router>,
    document.getElementById('root')
);
