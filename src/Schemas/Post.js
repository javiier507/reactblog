import yup from 'yup'

export const modelSchema = yup.object({
    post: yup.object({
        title: yup.string().min(4, "El título no puede tener menos de 4 caracteres")
            .required("El título es requerido"),
        content: yup.string().required('El post es requerido')
            .min(10, "El post no puede tener menos de 10 caracteres"),
    })
})
