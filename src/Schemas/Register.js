import yup from 'yup';

export const modelSchema = yup.object({
    user: yup.object({
        first_name: yup.string()
            .required('El nombre es requerido')
            .min(3, "El nombre no puede tener menos de 3 caracteres"),
        last_name: yup.string()
            .required('El apellido es requerido')
            .min(3, "El apellido no puede tener menos de 3 caracteres"),
        email: yup.string()
            .email('El formato del correo no es correcto')
            .required('El correo es requerido'),
        password: yup.string()
            .required('La contraseña es requerida')
            .min(6, "La contraseña no puede tener menos de 6 caracteres")
    })
})